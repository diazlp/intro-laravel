<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('form');
    }

    public function registered(Request $request) {
        //dd($request->all());
        $firstName = $request["firstName"];
        $firstName = strtoupper($firstName);

        $lastName = $request["lastName"];
        $lastName = strtoupper($lastName);

        return view('welcome',["firstName" => $firstName, "lastName" => $lastName]);
    }
}
