<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>SanberBook</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="welcome" method="POST">
        @csrf
        <label for="first_name">First name: <br><br></label>
        <input type="text" name="firstName" id="first_name" required> <br><br> 
	
	    <label for="last_name">Last name: <br><br></label> 
        <input type="text" name="lastName" id="last_name" required> <br><br> 
	
	    <label for="gender">Gender: <br><br></label>
        <input type="radio" name="gender" id="gender" value="male">Male<br>
        <input type="radio" name="gender" id="gender" value="female">Female<br>
        <input type="radio" name="gender" id="gender" value="others">Others<br><br> 

	   <label for="nationality">Nationality: <br><br></label>
        <select name="Nationality" id="nationality">
            <option value="indonesia">Indonesian</option>
            <option value="singapore">Singaporean</option>
            <option value="malaysia">Malaysian</option>
            <option value="australia">Australian</option>
        </select>
        <br><br> 

	   <label for="language">Language Spoken: <br><br></label>
        <input type="checkbox" id="language" name="language" value="Bahasa">Bahasa Indonesia<br>
        <input type="checkbox" id="language" name="language" value="English">English<br>
        <input type="checkbox" id="language" name="language" value="Other">Other<br><br> 

	   <label for="bio">Bio: <br><br></label>
        <textarea name="bio" id="bio" cols="20" rows="15"></textarea><br><br>

        <input type="submit" value="Sign Up">
    </form>
</body>

</html>
